<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'temps_lecture_description' => 'Permet d\'afficher un temps de lecture d\'un texte',
	'temps_lecture_nom' => 'Temps de lecture',
	'temps_lecture_slogan' => 'Afficher le temps de lecture',
);
