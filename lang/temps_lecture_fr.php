<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	//A
	'activer_automatique_explication' => 'Désactiver la case si l\'intégration est réalisée dans un squelette',
	'activer_automatique_label' => 'Affichage automatique',
	'activer_automatique_label_case' => 'Afficher automatiquement le temps de lecture',
	'avant_class_label' => 'Afficher avant la class',
	'avant_class_explication' => 'Le bloc du temps de lecture s\'affichera avant le tag html portant cette class.',

	// C
	'cfg_exemple' => 'Exemple',
	'cfg_exemple_explication' => 'Explication de cet exemple',
	'cfg_titre_parametrages' => 'Paramétrages',
	'cfg_titre_parametrages_explication' => '<b>Temps de lecture</b> ne s\'applique qu\'aux articles.',
	'compositions_refus_label' => 'Compositions à exclure',
	'compositions_refus_explication' => 'Ne pas afficher le temps de lecture sur les articles ayant les compositions suivantes.
	Liste des compositions séparées par une virgule, utiliser <b>defaut</b> pour exclure les articles sans compositions.',

	//E
	'explication_compositions_install' => 'Le plugin Compositions permettrait d\'affiner l\'affichage sur certaines pages',
	
	//M
	'mots_par_minute_explication' => 'Un adulte lit en moyenne 300 mots/minute',
	'mots_par_minute_label' => 'Vitesse de lecture par minute',
	
	//Q
	'quels_champs_label' => 'Quels champs de l\'article',
	'quels_champs_explication' => 'Compile les textes des champs séparés par une virgule.<br>
	Exemple : chapo,descriptif,texte,ps', 
	
	//T
	'temps_lecture_titre' => 'Temps de lecture',
	'temps_lecture_activer_objets_label' => 'Activer le temps de lecture pour les objets suivants',
	'temps_lecture_activer_objets_explication' => 'Fonctionne uniquement (pour le moment) sur les articles',
	'temps_lecture_estimation' => 'Temps de lecture estimé : @temps_lecture@ mn',
	'titre_page_configurer_temps_lecture' => 'Configurer l\'affichage du temps de lecture',

);
