<?php
/**
 * Fonctions au chargement du plugin Temps de lecture
 *
 * @plugin     Temps de lecture
 * @copyright  2020
 * @author     Anne-lise Martenot
 * @licence    GNU/GPL
 * @package    SPIP\Temps_lecture\Pipelines
 */
 
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Calcule le temps de lecture du texte d'un article donné
 *
 * @example
 * [Lecture en (#ID_ARTICLE|temps_lecture{article}) mn] -> Lecture en 4mn
 * @param intval $id_objet
 *     Identifiant de l'objet
 * @param string objet
 *     objet
 * @return intval
 *   Retourne le nombre de minutes si c'est valide, sinon rien.
 *
 **/
function temps_lecture($id_objet,$objet){
	include_spip('inc/config');
	$mots_par_minute = lire_config('temps_lecture/mots_par_minute', '250');	
	$_type_id = id_table_objet($objet); #id_article
	$table_sql = table_objet_sql($objet); #spip_articles

	if(intval($id_objet) AND isset($objet) AND intval($mots_par_minute)){
		$texte = sql_getfetsel("texte", $table_sql, "$_type_id=" . intval($id_objet));
		if(strlen($texte)>0){
		$str_word_count = str_word_count( strip_tags( textebrut( propre($texte) ) ) );


		// Eviter de doubler les notes
		$notes = charger_fonction('notes', 'inc');
		$notes('', 'depiler');

		$nb_minutes = round($str_word_count/$mots_par_minute);
				
			if($nb_minutes>0){		
				return $nb_minutes;
			} else {
				return false;
			}
		}
	}
	
	return false;
}

/**
 * Calcule le temps de lecture d'un texte donné
 *
 * @example
 * [Lecture en (#TEXTE|temps_lecture_texte) mn] -> Lecture en 4mn
 * @param string $texte
 *     texte à traiter
 * @return intval
 *   Retourne le nombre de minutes si c'est valide, sinon rien.
 *
 **/
function temps_lecture_texte($texte){
	include_spip('inc/config');
	$mots_par_minute = lire_config('temps_lecture/mots_par_minute', '250');	
	
	if(strlen($texte)>0 AND intval($mots_par_minute)){
		$str_word_count = str_word_count( strip_tags( textebrut( propre($texte) ) ) );

		// Eviter de doubler les notes
		$notes = charger_fonction('notes', 'inc');
		$notes('', 'depiler');

		$nb_minutes = round($str_word_count/$mots_par_minute);
		if($nb_minutes>0){		
			return $nb_minutes;
		} else {
			return false;
		}
	}
	
	return false;
}