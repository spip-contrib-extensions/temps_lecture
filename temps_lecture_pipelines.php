<?php
/**
 * Pipelines au chargement du plugin Temps de lecture
 *
 * @plugin     Temps de lecture
 * @copyright  2020
 * @author     Anne-lise Martenot
 * @licence    GNU/GPL
 * @package    SPIP\Temps_lecture\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/config');

/**
 * Inserer la CSS de temps_lecture dans le head public.
 *
 * @pipeline insert_head_css
 *
 * @param string $flux
 * 	Le contenu de la balise #INSERT_HEAD_CSS
 * @return string
 */
function temps_lecture_insert_head_css($flux) {
	if (defined('_TEMPS_LECTURE_NON')) {
		return $flux;
	}

	$temps_lecture_activer = lire_config('temps_lecture/temps_lecture_activer'); 
	if($temps_lecture_activer){
		$flux .= '<link rel="stylesheet" type="text/css" href="'.find_in_path('css/temps_lecture.css').'" />';
	}

	return $flux;
}


/**
 * Pipeline recuperer_fond
 * injecter le bloc temps de lecture après
 * analyse des fragments d'inclusion d'une page publique
 * et si le fond = 'content/article'
 *
 * @param array $flux
 * @return array
 **/
function temps_lecture_recuperer_fond($flux){
	
		$flux_fond = 'content/article';
		//plugin Zcore installé
		if(test_plugin_actif('Zcore') AND isset($GLOBALS['z_blocs'])){
			$flux_fond = $GLOBALS['z_blocs'][0].'/article';
		}
		
		if ($flux['args']['fond'] === $flux_fond ){
				
			$temps_lecture_activer = lire_config('temps_lecture/temps_lecture_activer'); 
			if($temps_lecture_activer){
				
				$id_objet = $flux['args']['contexte']["id_article"]; //54
				$objet =  $flux['args']['contexte']["type-page"]; //article
				$html = $flux['data']['texte']; //html du fragment
				$composition = $flux['args']['contexte']['composition']; //composition utilisée		
				if(!$composition){
					$composition = 'defaut';
				}
				//au cas ou ?
				//$pipeline_temps_lecture = pipeline('temps_lecture', '');
			
				//on affiche sur tout article sauf si sa composition est interdite
				$compo = true;
				//liste des compositions séparées par une virgule
				$composition_refus = lire_config('temps_lecture/compositions_refus'); 
				$array_composition_refus = explode(',',$composition_refus);
				//exclure si composition correspond, sinon on passe
				if(count($array_composition_refus)>0){
						if(in_array($composition,$array_composition_refus)){
							$compo = false;
						}
				}
				
				$texte = '';
				if(intval($id_objet) AND isset($objet)){
						
						$_type_id = id_table_objet($objet); // id_article
						$table_sql = table_objet_sql($objet); // spip_articles
						$trouver_table = charger_fonction('trouver_table', 'base');
						$desc = $trouver_table($table_sql);
						
						//verifier l'existence des champs demandés
						$select_valid = array();
						$quels_champs = lire_config('temps_lecture/quels_champs','texte');
						if(isset($quels_champs) AND strpos($quels_champs,',')){
							$champs = explode(',',$quels_champs);
						} else {
							$champs = array($quels_champs);
						}
						foreach($champs as $champ){
							if (isset($desc['field'][$champ])) {
								$select_valid[] = $champ;
							}
						}
						if(count($select_valid)>0){
							$row = sql_fetsel($select_valid, $table_sql, "$_type_id=" . intval($id_objet));
							foreach($row as $contenu){
								$texte .=  $contenu;
							}
						}
				}

				include_spip('temps_lecture_fonctions');
				$temps_lecture = temps_lecture_texte($texte);
				$class = lire_config('temps_lecture/apres_class'); //"div.texte";
				if($compo AND $temps_lecture > 0 AND strlen($class)>0){
					$some_html = "<div class='temps_lecture'><span>"
					._T('temps_lecture:temps_lecture_estimation',array('temps_lecture' => $temps_lecture))
					."</span></div>";
					$temps_lecture_ajoute = temps_lecture_ajoute($html, $class, $some_html);
					$flux['data']['texte'] = $temps_lecture_ajoute;
				}
				//$flux['data']['texte'] = 'composition = '.$composition;
			}
		}
	
	return $flux;
}


/**
 * ajouter le temps de lecture dans le fragment html avant le tag ayant l'attribut $class
 *
 * @param string $html  
 *		le fragment html (avec son DOM)
 * @param string $class
 *		la class du tag après lequel insérer la chaine
 * @param string $some_html 
 *		la chaine html à insérer
 * @return string
 **/
function temps_lecture_ajoute($html, $class, $some_html) {	
	if(isset($html) AND isset($some_html) AND isset($class)){
		include_spip('inc/querypath');
		if (function_exists("html5qp")){
			$qp = html5qp($html);	
			$qp->find($class)->before($some_html);
			return $qp->innerHTML5();
		} 
	} else {
		return $html;
	}
}