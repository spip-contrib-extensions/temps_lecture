# temps_lecture

Documentation sur https://contrib.spip.net/Temps-de-lecture

Author: Anne-lise Martenot

## Afficher le temps de lecture d'un texte

nécessite le plugin querypath
nécessite le plugin saisies
	
utilise optionnellement le plugin Zcore qui intègre par defaut le répertoire du contenu de type content/article nécessaire
utilise optionnellement le plugin compositions


-------
###  Prérequis
Les squelettes HTML qui affichent la page d’un article doivent inclure un « content/article.html » comme par exemple #INCLURE{fond=content/article,id_article} dans une boucle article et avoir une class css .texte pour le div contenant le texte.

Si vous utilisez le plugin Zcore le plugin Temps de lecture intègre d’office la page « content/article » avec son div.texte ou sinon récupère le 1er répertoire de votre configuration personnelle des z_blocs de l’array $GLOBALS['z_blocs'] qui pourrait par exemple être main ou contenu.
En ajoutant le plugin compositions, vous pouvez affiner l’insertion du temps de lecture sur certaines pages d’articles, même si la composition d’un article est de type content/article-votre_composition.

Sinon, pour vous passer de ces prérequis, inscrire le filtre directement dans votre squelette sur une balise #TEXTE.
[Temps de lecture estimé : (#TEXTE|temps_lecture_texte) mn]

-------
### Méthode
Le plugin propose pour le moment 2 méthodes pour afficher le temps de lecture:

Méthode 1/ Rien à faire : sauf activer le plugin qui insère automatiquement le bloc "temps de lecture" sur toutes les pages articles 
- possibilité de configurer la class (par defaut div.texte) devant laquelle s'insère le bloc "temps de lecture".
- possibilité de configurer les pages article disposant de Compositions pour ne pas afficher le bloc "temps de lecture".

Méthode 2/ 
Utiliser ce code d'exemple dans le squelette de votre choix
[Temps de lecture estimé : (#TEXTE|temps_lecture_texte) mn]
dans ce cas décochez l'insertion automatique dans la configuration.

-------
### Surcharges possibles
Pour modifier le texte vous pouvez créer un fichier lang/temps_lecture_fr.php dans votre repertoire de plugin ou de squelettes
et inscrire la nouvelle chaine en respectant l'exemple
```
	'temps_lecture_estimation' => 'Temps de lecture estimé : @temps_lecture@ mn',
```
	
Pour modifier l'affichage, vous pouvez créer un fichier css/temps_lecture.css dans votre repertoire de plugin ou de squelettes

### Todo
intégrer différents scripts js au choix dans la config tel le défilement de la page pour le temps de lecture.
